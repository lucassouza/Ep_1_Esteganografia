#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include "ClasseImagem.hpp"
#include "TopFunc.hpp"
#include "DecodificarPPM.hpp"

using namespace std;

//struct para filtro RGB


typedef struct {
    char r, g, b;
} pixel;

int main(){

	//Printa o cabeçalho.
	cabecalho();
	//Declarando variáveis.
	int opcao = 0, contadorB = 0, contador = 0;
	string line;
	//Iniciando Menu.
	cout << "Este programa decodifica imagens que passaram pelo metodo de Esteganografia." << endl;
	cout << "Escolha uma das opções abaixo:" << endl;
	cout << "\n\n       1 -> Decodificar uma imagem '.pgm.'" << endl;
	cout << "       2 -> Decodficar uma imagem '.ppm'." << endl;
	cout << "       3 -> Sair do Programa." << endl;


	//Laço infinito até que o usuário digite um numero válido.
	while( true ){
		cout << "  ";
		cin >> opcao;
		
		if((opcao == 3)){
	
		    cout << "Saindo do programa." << endl;
			exit(0);
		}

		if((opcao == 1)||(opcao == 2)){
			break;
		}

		cout << "Digite um numero valido!" << endl;	

	}

	//Opção 1 - Decodificar uma imagem '.pgm'.
	
	if(opcao == 1){
		
	//Dizer o local e nome do arquivo.
	cout << "\nDigite o local e o nome da imagem.pgm." << endl;
	cout << "Exemplo: /home/usuario/Downloads/imagem1.pgm" << endl;

	string nome_arquivo_entrada;
	cin >> nome_arquivo_entrada;

	//Abrindo o arquivo solicitado.
	ifstream Arquivo (nome_arquivo_entrada.c_str());

		//Testa a abertura do Arquivo. Caso ele não abra, o programa será fechado.
		if(!Arquivo ) {
			
			cout << "Não foi possivel abrir o arquivo escolhido. O programa será finalizado."<< endl;
			return -1;

		}

	//Caso o Arquivo abra normalmente, o programa prossegue pegando a primeira linha do próprio. Seu número mágico.
	string NumeroMagico;
	getline(Arquivo, NumeroMagico);
	
	//Testa o número mágico para saber se o formato realmente é PGM.
		if(NumeroMagico != "P5"){

			cout << "O arquivo escolhido não se trata de uma imagem PGM. O programa será finalizado." << endl;
			return -1;
		}

	//Caso o número mágico seja P5, o programa prossegue.
	
	//Extraindo a próxima linha. De acordo com os arquivos utilizados para a criação do programa, no '.pgm', segue-se um padrão
	//Em que na segunda linha do arquivo se encontra um comentário com a posição inicial da mensagem.
	
	string line;
	getline(Arquivo, line);
	
	//Chama a função que devolve uma string com a posição inicial da mensagem.
	line = posicao(line);

	//Transforma a string em um inteiro.
	int Posicao_Inicial = atoi(line.c_str());

	//Extraindo a próxima linha. De acordo com os arquivos utilizados para a criação do programa, no '.pgm', segue-se um padrão
	//Em que na terceira linha do arquivo se encontra a altura e a largura da imagem.
	
	getline(Arquivo, line);

	//For que extrai duas strings contendo os valores de altura e largura.

	string temp = "";
	string tempB = "";

	for(contador = 0 ;  contador <= line.size() ; contador++){
		
		if(contadorB == 0){
				temp = temp + line[contador];
			}

		if(contadorB == 1){
				tempB = tempB + line[contador];
				
		}

		if(line[contador] == (' ') || ('\0')){
			contadorB++;
			if(contador == 2){
				break;
				}
		}
		
		}

	//transforma as duas strings em inteiros.
	int Altura = atoi(temp.c_str());
	int Largura = atoi(tempB.c_str());

	//Extraindo a próxima linha. De acordo com os arquivos utilizados para a criação do programa, no '.pgm', segue-se um padrão
	//Em que na quarta linha do arquivo se encontra a escala de cor do arquivo.

	getline(Arquivo, line);
	
	int CorMax = atoi(line.c_str());

	
	//Alocar a memória para a criação do objeto Imagem.
	Imagem * ImagemPGM = new Imagem(Posicao_Inicial, Altura, Largura, CorMax);

	//Testar a alocação de memória
	if(!ImagemPGM){
			cout << "Falha na alocacao. O programa sera fechado!" << endl;
			return -1;
	}	

	//Calcular dimensao a partir do objeto ImagemPGM.
	int dimens = ImagemPGM->calculaDimensao();
	
	//Vai até a posição incial da mensagem.
	Arquivo.seekg(Posicao_Inicial, ios_base::cur);

	//Chars para decodificação.
	char saida;
	char byte;
	char ch;

	//Funçoes para decodificação.
	
	cout << "\n\n";
		for(int bb = 0; bb <= dimens; bb++){

			saida = 0x00;	
	

			for( int as = 0 ; as <= 6; as++ ){	
	
				Arquivo.get(ch);
				byte = ch & 1;
				saida = saida | byte;
				saida = saida << 1;
				Posicao_Inicial++;
				
			}

			Arquivo.get(ch);
			byte = ch & 1;
			saida = saida | byte;
			Posicao_Inicial++;

			if (saida == '#' && bb > 0){
			cout << endl;
			break;
			}
		printf("%c", saida);


		}
	
	Arquivo.close();
	//Considerações finais.
	cout << "\n\nA mensagem foi decodificada com sucesso!\nFinalizando Programa." << endl;
	cout << "***Ultima modificação 26 de abril de 2016***" << endl;
	cout << "Por: Lucas S. Souza\n\n" << endl;

	delete(ImagemPGM);
	Arquivo.close();

	
}

	//Opção decodificar arquivo '.ppm'.
	if(opcao == 2){
	
		//Dizer o local e nome do arquivo.
	cout << "\nDigite o local e o nome da imagem.ppm." << endl;
	cout << "Exemplo: /home/usuario/Downloads/mensagem.ppm" << endl;

	string nome_arquivo_entrada;
	cin >> nome_arquivo_entrada;

	//Abrindo o arquivo solicitado.
	ifstream Arquivo (nome_arquivo_entrada.c_str());

		//Testa a abertura do Arquivo. Caso ele não abra, o programa será fechado.
		if(!Arquivo ) {
			
			cout << "Não foi possivel abrir o arquivo escolhido. O programa será finalizado."<< endl;
			return -1;

		}

	//Caso o Arquivo abra normalmente, o programa prossegue pegando a primeira linha do próprio. Seu número mágico.
	string NumeroMagico;
	getline(Arquivo, NumeroMagico);
	
	//Testa o número mágico para saber se o formato realmente é PPM.
		if(NumeroMagico != "P6"){

			cout << "O arquivo escolhido não se trata de uma imagem PGM. O programa será finalizado." << endl;
			return -1;
		}

	//Caso o número mágico seja P6, o programa prossegue.
	
	//Extraindo a próxima linha. De acordo com os arquivos utilizados para a criação do programa, no '.ppm', segue-se um padrão
	//Em que na segunda linha do arquivo se encontra um comentário. Neste caso ele será ignorado.
	string line;
	getline(Arquivo, line);
	
	//Extraindo a próxima linha. De acordo com os arquivos utilizados para a criação do programa, no '.ppm', segue-se um padrão
	//Em que na terceira linha do arquivo se encontra a altura e a largura da imagem.
	
	getline(Arquivo, line);

	//For que extrai duas strings contendo os valores de altura e largura.

	string temp = "";
	string tempB = "";

	for(contador = 0 ;  contador <= line.size() ; contador++){
		
		if(contadorB == 0){
				temp = temp + line[contador];
			}

		if(contadorB == 1){
				tempB = tempB + line[contador];
				
		}

		if(line[contador] == (' ') || ('\0')){
			contadorB++;
			if(contador == 2){
				break;
				}
		}
		
		}

	//transforma as duas strings em inteiros.
	int Altura = atoi(temp.c_str());
	int Largura = atoi(tempB.c_str());

	//Extraindo a próxima linha. De acordo com os arquivos utilizados para a criação do programa, no '.ppm', segue-se um padrão
	//Em que na quarta linha do arquivo se encontra a escala de cor do arquivo.

	getline(Arquivo, line);
	
	int CorMax = atoi(line.c_str());

	
	//Alocar a memória para a criação do objeto Imagem.
	Imagem * ImagemPPM = new Imagem(Altura, Largura, CorMax);

	//Testar a alocação de memória
	if(!ImagemPPM){
			cout << "Falha na alocacao. O programa sera fechado!" << endl;
			return -1;
	}	

	//Calcular dimensao a partir do objeto ImagemPPM;
	int dimens = ImagemPPM->calculaDimensao();
	
	
	//Alocando memoria para a imagem;
	  pixel** matrix;
  
  	matrix = new pixel*[Altura];
  	for (int i=0; i<Altura; i++) {
      matrix[i] = new pixel[Largura];
		}
		
	

	for(int i=0; i < Altura ; i++){
		for(int a=0; a < Largura; a++){
			Arquivo.get(matrix[i][a].r);
			Arquivo.get(matrix[i][a].g);
			Arquivo.get(matrix[i][a].b);
		}
	}

 	
	//Abrindo Arquivo de Saída.
	cout << "\nDigite o local e o nome do arquivo a ser criado. Não esqueça de colocar a extensão '.ppm'" << endl;
	cout << "Exemplo: /home/usuario/decodificada/mensagem1decodificada.ppm" << endl;
	string Arquivo_Criado;
	cin >> Arquivo_Criado;

	ofstream ArquivoOut(Arquivo_Criado.c_str());
	//Testando abertura Válida de Arquivo
	if(!ArquivoOut){
		cout << "O arquivo não pôde ser criado. Fechando o programa\n";
		return -1;
		}
	//Printando no Arquivo os dados da Imagem
	ArquivoOut << NumeroMagico << endl;
	ArquivoOut << Altura << ' ' << Largura << endl;
	ArquivoOut << CorMax << endl; 

	//Menu para escolha do Filtro
	int cor;

	cout << "Escolha 1 para Vermelho. Escolha 2 para Verde. Escolha 3 para o Azul" << endl;
	cout << "       Escolha 1 para Vermelho." << endl;
	cout << "       Escolha 2 para Verde." << endl;
	cout << "       Escolha 3 para o Azul." << endl;
	
	//Laço Infinito para que o usuario digite uma opção válida
	while( true ){
		cin >> cor;
			if((cor == 1)||(cor == 2)||(cor == 3)){
				break;
			}
	    cout << "Digite uma opção válida" << endl;
	}
	//Filtro 1 - Vermelho
	if(cor == 1){

	for(int i=0; i < Altura ; i++){
		for(int a=0; a < Largura; a++){
			matrix[i][a].r = matrix[i][a].r;
			matrix[i][a].g = 0;
			matrix[i][a].b = 0;
			ArquivoOut << matrix[i][a].r;
			ArquivoOut << matrix[i][a].g;
			ArquivoOut << matrix[i][a].b;
		}
	}

	}
	//Filtro 2 - Verde
	if(cor == 2){

	for(int i=0; i < Altura ; i++){
		for(int a=0; a < Largura; a++){
			matrix[i][a].r = 0;
			matrix[i][a].g = matrix[i][a].g;
			matrix[i][a].b = 0;
			ArquivoOut << matrix[i][a].r;
			ArquivoOut << matrix[i][a].g;
			ArquivoOut << matrix[i][a].b;
		}
	}

	}
	//Filtro 3 - Azul
	if(cor == 3){

	for(int i=0; i < Altura ; i++){
		for(int a=0; a < Largura; a++){
			matrix[i][a].r = 0;
			matrix[i][a].g = 0;
			matrix[i][a].b = matrix[i][a].b;
			ArquivoOut << matrix[i][a].r;
			ArquivoOut << matrix[i][a].g;
			ArquivoOut << matrix[i][a].b;
		}
	}

	}
	//Considerações finais
	cout << "\n\nA mensagem foi decodificada com sucesso!\nFinalizando Programa." << endl;
	cout << "***Ultima modificação 26 de abril de 2016***" << endl;
	cout << "Por: Lucas S. Souza\n\n" << endl;

delete(ImagemPPM);	
ArquivoOut.close();
Arquivo.close();

}

return 0;
} 
