#ifndef DECODIFICARPPM_H
#define DECODIFICARPPM_H

class DecodificarPPM{

private:
	//Atributos
	char red;
	char green;
	char blue;

	
public:

	DecodificarPPM();

	void setRed(char red);
	char getRed();
	void setGreen(char green);
	char getGreen();
	void setBlue(char blue);
	char getBlue();

};

#endif
